CREATE TABLE `users` (
	`id`         INT          NOT NULL UNIQUE AUTO_INCREMENT,
	`username`   VARCHAR( 32) NOT NULL UNIQUE,
	`password`   VARCHAR(255) NOT NULL,
	`firstname`  VARCHAR( 16) DEFAULT NULL,
	`lastname`   VARCHAR( 16) DEFAULT NULL,
	`email`      VARCHAR(256) DEFAULT NULL,
	`created_at` DATETIME     DEFAULT CURRENT_TIMESTAMP,
	
	PRIMARY KEY (`id`)
);
