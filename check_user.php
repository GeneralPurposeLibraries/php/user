<?php

function check_user()
{
	if (!isset($_SESSION['user_id'])) {
		header('Location: /user/login/');
		exit;
	}
	else {
		return $_SESSION['user_id'];
	}
}
